
def compression(string):

    result = ""
    count = 1

    #put the first letter in 
    result += string[0]

    #Traverse the length of the string and skip one position
    # Minus one because first index is already added
    for i in range(len(string)-1):
        #check here to see if the current index is equal to the next index 
        #if so, increment the counter 
        if(string[i] == string[i+1]):
            count+=1
        #handles the case where there is no repetition
        else:
            if(count > 1):
                #Ignore if no repeats
                result += str(count)
            #add the next letter after the count
            result += string[i+1]
            count = 1
    #add the last index to the string
    if(count > 1):
        result += str(count)
    return result

print(compression('bbcceeee'))
print(compression('aaabbbcccaaa'))
print(compression('a'))